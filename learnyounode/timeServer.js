var net = require('net');
var server = net.createServer(function (socket){
	var date = new Date();
	var data = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()+' '+addZero(date.getHours())+':'+addZero(date.getMinutes())+'\n';
	socket.end(data);
})
server.listen(process.argv[2]);

function addZero(n){
	return (n < 10 ? '0' : ' ') + n
}
