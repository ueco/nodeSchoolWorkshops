module.exports = function(inputWords){
	var returnObj = {};
	returnObj[inputWords[0]] = 1;
	inputWords.reduce(function(acc, curr){
		if(!returnObj[curr]) return returnObj[curr] = 1;
		else return returnObj[curr]++;
	})
	return returnObj;
}
